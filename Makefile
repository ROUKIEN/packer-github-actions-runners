TPL=template.json

.PHONY: build
build: clean validate ## builds the template
	packer build $(TPL)

.PHONY: validate
validate: ## validates the template
	packer validate $(TPL)

.PHONY: clean
clean: ## cleans the workspace
	rm -rf ./output-vagrant
