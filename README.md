# Packer tests

This repo builds a vagrant box using packer that will be used as a Github Actions self-hosted runner. Provisioning stuff is performed with ansible.

## Requirements

> Since ansible will be executed on your actual machine, it is highly recommended to run the project on a linux host.

The Builder requires the following softwares:

 * virtualbox
 * vagrant
 * packer
 * ansible

You'll also need to install `jmespath` on the ansible control node. It is required to parse some json APIs responses

## Building the template

> The resulting vagrant box will be available in the `output-vagrant` folder.



Simply run ```make build```
